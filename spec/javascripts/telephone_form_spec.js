(function($){
  var ajaxStub;
  var userForm;
  var successJson;
  var html = '' +
    '<ul class="users-grid">' +
    '  <li class="user-row" data-user="2">'  +
    '    <a href="#" class="user-add-telephone">add</a>' +
    '  <li>' +
    '</ul>' +
    '<div id="telephone-form-group" style="display:none;">' +
    '<form type="post" id="telephone-form" action="/telephone_number">' +
    '  <input type="hidden" name="user_id" value="2">' +
    '  <input type="text" name="telephone_number" value="11 12345678">' +
    '  <input type="submit" name="commit" value="cadastrar">' +
    '</form>' +
    '</div>' +
    '<ul id="number-list"></ul>';

  describe("Telephone Controller", function(){
    var error_json = { responseText: "{\"errors\":{\"telephone_number\":[\"can't be blank\"]}}"};
    var alert_msg = "";
    successJson = {id:1,
                      user_id:4,
                      telephone_number:"11-35555555",
                      created_at:"2015-06-13T19:27:08.051-03:00",
                      updated_at:"2015-06-13T19:27:08.085-03:00"
                  };

    beforeEach(function() {
      jasmine.getFixtures().set(html);
      ajaxStub = sinon.stub(jQuery, "ajax");
      spyOn(window, 'alert').and.callFake(function(msg) {});
      telephoneForm = new $.userRegister.telephone_form_controller();
      telephoneForm.init();
    });

    afterEach(function(){
      jasmine.getFixtures().cleanUp();
      $.ajax.restore();
    });

    describe("Form", function(){

      beforeEach(function(){
        $(".user-add-telephone").click();
      });

      it('should send ajax request to user url with form data', function(){
        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect(ajaxStub.calledOnce).toBeTruthy();
        var args = ajaxStub.getCall(0).args[0];
        expect(args.url).toEqual('/telephone_number');
        expect(args.type).toEqual('POST');
        expect(args.data.telephones.user_id).toEqual("2");
        expect(args.data.telephones.telephone_number).toEqual("11 12345678");
      });

      it('should render a success message if telephone is saved', function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect(window.alert).toHaveBeenCalledWith('Telefone cadastrado com sucesso');
      });

      it('should close form when telephone is saved', function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect($('#telephone-form-group')).not.toBeVisible();
      });

      it('should remove user_id', function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect($('input[name="user_id"]').val()).toEqual('');
      });

      it('should render a success message if telephone is not saved', function(){
        // Arrange
        ajaxStub.yieldsTo("error", error_json);

        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect(window.alert).toHaveBeenCalledWith('Ops houve um erro');
      });

      it("should set field-with-error if user not valid", function(){
        // Arrange
        ajaxStub.yieldsTo("error", error_json);

        // Act
        $('input[type="submit"]', $('#telephone-form')).click();

        // Assert
        expect($('input[name="telephone_number"]')).toHaveClass('field-with-error');
      });

      describe('Update numbers list', function(){

        it("should get number data", function(){
          // Arrange
          ajaxStub.yieldsTo("success", successJson);

          // Act
          $('input[type="submit"]', $('#telephone-form')).click();

          // Assert
          expect($('li', $("#number-list")).length).toEqual(1);
        });
      });
    });

    describe("Add Telephone link", function(){
      it("should show form when clicked", function(){
        // Arrange
        $(".user-add-telephone").click();

        // Assert
        expect($('#telephone-form-group')).toBeVisible();
      });

      it("should set user_id", function(){
        // Act
        $(".user-add-telephone").click();

        // Assert
        expect($('input[name="user_id"]').val()).toEqual('2');
      });
    });
  });

})(jQuery);
