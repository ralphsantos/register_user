(function($){
  var ajaxStub;
  var userForm;
  var html = '' +
    '<a href="#" id="add-new-user"></a>' +
    '<ul class="users-grid">' +
    '  <li class="user-row" data-user="2">'  +
    '    <a href="#" class="user-show-telephone">add</a>' +
    '  <li>' +
    '</ul>' +
    '<div id="user-form-group" style="display:none;">' +
    '  <form type="post" id="user-form" action="/user">' +
    '    <input type="text" name="email" id="email" value="person2@example.com">' +
    '    <input type="text" name="name" id="name" value="Paola">' +
    '    <input type="text" name="address" id="address" value="Rua dos pinheiros">' +
    '    <input type="text" name="number" id="number" value="500">' +
    '    <input type="text" name="neighbourhood" id="neighbourhood" value="Pinheiros">' +
    '    <input type="text" name="complement" id="complement" value="Casa 3">' +
    '    <input type="text" name="uf" id="uf" value="SP">' +
    '    <input type="text" name="city" id="city" value="São Paulo">' +
    '    <input type="text" name="country" id="country" value="Brasil">' +
    '    <input type="submit" name="commit" value="cadastrar">' +
    '  </form>' +
    '</div>' +
    '<div id="user-number-group" style="display:none;" class="form-group">' +
    '  <ul></ul>' +
    '</div>';



  describe("User Form", function(){
    var error_json = { responseText: "{\"errors\":" +
                        "{\"email\":[\"can't be blank\"],"+
                        "\"name\":[\"can't be blank\"],"+
                        "\"address\":[\"can't be blank\"],"+
                        "\"number\":[\"can't be blank\"],"+
                        "\"neighbourhood\":[\"can't be blank\"],"+
                        "\"city\":[\"can't be blank\"],"+
                        "\"country\":[\"can't be blank\"],"+
                       "\"uf\":[\"can't be blank\"]}}"
                     };
    var alert_msg = "";

    beforeEach(function() {
      jasmine.getFixtures().set(html);
      ajaxStub = sinon.stub(jQuery, "ajax");
      userForm = new $.userRegister.user_form_controller();
      userForm.init();
      spyOn(window, 'alert').and.callFake(function(msg) {});
    });

    afterEach(function(){
      jasmine.getFixtures().cleanUp();
       $.ajax.restore();
    });

    describe("Show User Telephones", function(){
      successJson = [{id:1,
                        user_id:4,
                        telephone_number:"11-35555555",
                        created_at:"2015-06-13T19:27:08.051-03:00",
                        updated_at:"2015-06-13T19:27:08.085-03:00"
                    },
                    {id:2,
                      user_id:4,
                      telephone_number:"11-45555555",
                      created_at:"2015-06-13T19:27:08.986-03:00",
                      updated_at:"2015-06-13T19:27:08.995-03:00"
                    },
                    {id:3,
                      user_id:4,
                      telephone_number:"11-55555555",
                      created_at:"2015-06-13T19:27:09.346-03:00",
                      updated_at:"2015-06-13T19:27:09.353-03:00"
                    }];

      it("should get numbers via ajax", function(){
        // Act
        $(".user-show-telephone").click();

        // Arrange
        expect(ajaxStub.calledOnce).toBeTruthy();
        var args = ajaxStub.getCall(0).args[0];
        expect(args.url).toEqual('/user-telephones');
        expect(args.type).toEqual('GET');
        expect(args.data.user_id).toEqual(2);
      });

      it("should show telephones", function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $(".user-show-telephone").click();

        // Assert
        expect($("#user-number-group")).toBeVisible();
      });

      it("should render telephones", function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $(".user-show-telephone").click();

        // Assert
        expect($("ul", $("#user-number-group")).html()).not.toEqual('');
      });

      it('should show message if error', function(){
        // Arrange
        ajaxStub.yieldsTo("error");

        // Act
        $(".user-show-telephone").click();

        // Assert
        expect(window.alert).toHaveBeenCalledWith('Ops houve um erro, tente novamente');
      });

      it('should show formated date', function(){
        // Arrange
        ajaxStub.yieldsTo("success", successJson);

        // Act
        $(".user-show-telephone").click();

        // Assert
        var date = $(".created_at").last();
        expect(date.html()).toEqual('Criado em: 13/5/2015');
      });
    });

    describe("Add User Link", function(){
      it("should show form fields when link is clicked", function(){
        // Act
        $('#add-new-user').click();

        // Assert
        expect($('#user-form-group')).toBeVisible();
      });
    });

    describe("Form", function(){
      var userData = {id: 9,
                      address: "Rua Giovanni mário",
                      city: "São Paulo",
                      complement: '',
                      country: "Brasil",
                      email: "Teste@gmail.com",
                      name: "rafael",
                      neighbourhood:"Américanopolis",
                      number: "12",
                      uf: "SP"
                    };

      beforeEach(function(){
        $('#add-new-user').click();
      });

      it('should send ajax request to user url with form data', function(){
        // Act
        $('input[type="submit"]', $("#user-form")).click();

        // Assert
        expect(ajaxStub.calledOnce).toBeTruthy();
        var args = ajaxStub.getCall(0).args[0];
        expect(args.url).toEqual('/user');
        expect(args.type).toEqual('POST');
        expect(args.data.user.email).toEqual("person2@example.com");
        expect(args.data.user.name).toEqual("Paola");
        expect(args.data.user.address).toEqual("Rua dos pinheiros");
        expect(args.data.user.number).toEqual("500");
        expect(args.data.user.neighbourhood).toEqual("Pinheiros");
        expect(args.data.user.complement).toEqual("Casa 3");
        expect(args.data.user.uf).toEqual("SP");
        expect(args.data.user.city).toEqual("São Paulo");
        expect(args.data.user.country).toEqual("Brasil");
      });

      it('should render a success message if user is saved', function(){
        // Arrange
        ajaxStub.yieldsTo("success", userData);

        // Act
        $('input[type="submit"]', $("#user-form")).click();

        // Assert
        expect(window.alert).toHaveBeenCalledWith('Usuário cadastrado com sucesso');
      });


      it('should render a success message if user is not saved', function(){
        // Arrange
        ajaxStub.yieldsTo("error", error_json);

        // Act
        $('input[type="submit"]', $("#user-form")).click();

        // Assert
        expect(window.alert).toHaveBeenCalledWith('Ops houve um erro');
      });

      it('should close form if success', function(){
        // Arrange
        ajaxStub.yieldsTo("success", userData);

        // Act
        $('input[type="submit"]', $("#user-form")).click();

        // Assert
        expect($('.user-form-group')).not.toBeVisible();
      });

      it("should set field-with-error if user not valid", function(){
        // Arrange
        ajaxStub.yieldsTo("error", error_json);

        // Act
        $('input[type="submit"]', $("#user-form")).click();

        // Assert
        expect($('input[name="email"]')).toHaveClass('field-with-error');
        expect($('input[name="name"]')).toHaveClass('field-with-error');
        expect($('input[name="address"]')).toHaveClass('field-with-error');
        expect($('input[name="number"]')).toHaveClass('field-with-error');
        expect($('input[name="neighbourhood"]')).toHaveClass('field-with-error');
        expect($('input[name="uf"]')).toHaveClass('field-with-error');
        expect($('input[name="city"]')).toHaveClass('field-with-error');
        expect($('input[name="country"]')).toHaveClass('field-with-error');
      });

      describe('Update User List', function(){

        it("should get user data", function(){
          // Arrange
          ajaxStub.yieldsTo("success", userData);

          // Act
          $('input[type="submit"]', $("#user-form")).click();

          // Assert
          expect($('.user-row').length).toEqual(2);
        });
      });

    });
  });

})(jQuery);
