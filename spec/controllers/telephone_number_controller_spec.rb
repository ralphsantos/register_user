require 'rails_helper'

RSpec.describe TelephoneNumberController, type: :controller do
  describe "create" do
    it "should create telephone number" do
      # Arrange
      telephone_params = FactoryGirl.attributes_for(:telephone_number)

      # Act/Assert
      post :create, format: "json" , telephones: telephone_params
    end

    it "should return unprocessable entety if telephone is not valid" do
      telephone_params = FactoryGirl.attributes_for(:telephone_number, telephone_number: nil)
      post :create, format: "json" , telephones: telephone_params
      assert_response :unprocessable_entity
    end

    it "should redirect to root path if a html request" do
      telephone_params = FactoryGirl.attributes_for(:telephone_number)
      post :create, format: "html" , telephones: telephone_params
      response.should redirect_to root_path
    end
  end
end
