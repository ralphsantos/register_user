require 'rails_helper'

RSpec.describe UserController, type: :controller do
  render_views

  describe "index" do
    it "should show a list of all users" do
      FactoryGirl.create(:user)
      get :index, format: "html"

      assert_select ".user-row", 1
    end

    it "should have link to add new user telephone" do
      get :index, format: "html"

      assert_select '#add-new-user', 1
    end
  end

  describe "create" do
    before(:each) do
      telephone1 = FactoryGirl.build(:telephone_number)
      telephone2 = FactoryGirl.build(:telephone_number)
      @user_params = FactoryGirl.attributes_for(:user)
      @telephone_params = [telephone1.telephone_number, telephone2.telephone_number]
    end

    it "should create user without telephone number" do
      # Act/Assert
      assert_difference 'User.count', 1 do
        post :create, format: "json" , user: @user_params
      end

      assert_response :created
    end

    it "should return unprocessable entety if user is not valid" do
      invalid_user = FactoryGirl.attributes_for(:user, address: nil)
      post :create, format: "json" , user: invalid_user
      assert_response :unprocessable_entity
    end

    it "should redirect to root path if a html request" do
      post :create, format: "html" , user: @user_params
      response.should redirect_to root_path
    end
  end
end
