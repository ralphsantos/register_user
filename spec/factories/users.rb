FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    name 'Paola'
    address 'Rua dos pinheiros'
    number '500'
    neighbourhood 'Pinheiros'
    complement 'Casa 3'
    city 'São Paulo'
    country  'Brasil'
    uf  'SP'
  end

end
