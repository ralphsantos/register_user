FactoryGirl.define do
  factory :telephone_number do
    sequence(:telephone_number) { |n| "11-#{n}5555555" }
  end
end
