require 'rails_helper'

RSpec.describe TelephoneNumber, type: :model do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  it "should be invalid if telephone number has less the 8 digits" do
    telephone = FactoryGirl.build(:telephone_number, user: @user, telephone_number: '11111')
    expect(telephone).not_to be_valid
  end

  it "should be invalid if telephone number is blank" do
    telephone = FactoryGirl.build(:telephone_number, user: @user, telephone_number: nil)
    expect(telephone).not_to be_valid
  end

  it "should be valid if passed a normal telephone" do
    telephone = FactoryGirl.build(:telephone_number)
    expect(telephone).to be_valid
  end
end
