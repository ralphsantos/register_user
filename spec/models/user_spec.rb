require 'rails_helper'

RSpec.describe User, type: :model do
  it "should be invalid if email is blank" do
    user = FactoryGirl.build(:user, email: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if name is blank" do
    user = FactoryGirl.build(:user, name: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if address is blank" do
    user = FactoryGirl.build(:user, address: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if number is blank" do
    user = FactoryGirl.build(:user, number: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if neighbourhood is blank" do
    user = FactoryGirl.build(:user, neighbourhood: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if city is blank" do
    user = FactoryGirl.build(:user, city: nil)
    expect(user).not_to be_valid
  end

  it "should be invalid if contry is blank" do
    user = FactoryGirl.build(:user, country: nil)
    expect(user).not_to be_valid
  end

  it "should be valid with complement" do
    user = FactoryGirl.build(:user)
    expect(user).to be_valid
  end

  it "should be valid without complement" do
    user = FactoryGirl.build(:user, complement: nil)
    expect(user).to be_valid
  end
end
