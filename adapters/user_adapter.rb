class UserAdapter
  def initialize(params)
    @telephones = params[:telephone_numbers]
    @user_data = params[:user]
  end

  def adapt
    user = User.new(@user_data)
    user.telephone_numbers = adapt_telephones(@telephones)
    return user
  end

  def adapt_telephones(telephone_data)
    telephone_array = []
    telephone_data.each do |telephone|
      telephone_array << TelephoneNumber.new(telephone)
    end
    telephone_array
  end
end
