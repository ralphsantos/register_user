 Teste Desenvolvedor Rails TruckPad *

Criar um app Ruby on Rails do zero, com banco de dados, para cadastro de usuário.
O usuário tem que poder ter vários telefones e um endereço.
Monte o endereço e telefones do jeito que achar melhor.
Coloque as validações básicas e plugins js que achar pertinente.
Você deve fazer as migrations e usar as Gems que achar pertinentes.
Não há necessidade de login ou autenticação, apenas o CRUD de cadastro completo.
Use a versão de Ruby e Rails que preferir.
Não precisa fazer deploy, apenas faça o upload do código em um repositório no Github ou Bitbucket.

    Pontos extras: 
Crie um helper method para exibir a data de criação do registro, se a data for menor que dois dias exibir no formato  "aproximadamente 10 horas" ou "1 dia", caso a data seja maior que dois dias exibir a data por extenso "10/06/2015".
Testes unitário e de feature. Pode utilizar tanto o minitest ou rspec, para teste de feature sugiro o capybara.


Commits separados e bem explicativos