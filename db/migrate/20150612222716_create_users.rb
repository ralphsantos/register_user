class CreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      t.string :email,              null: false, default: ""
      t.string :name,               null: false, default: ""
    ## User Address
      t.string :address
      t.string :number
      t.string :neighbourhood
      t.string :complement
      t.string :uf
      t.string :city
      t.string :country

      t.timestamps
    end

    add_index :users, :email,                unique: true
  end
end
