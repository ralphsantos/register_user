class CreateTelephoneNumbers < ActiveRecord::Migration
  def change
    create_table :telephone_numbers do |t|
      t.integer :user_id
      t.string :telephone_number
      t.timestamps null: false
    end
  end
end
