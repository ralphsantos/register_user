(function($){

  $.userRegister = $.userRegister || {};

  $.userRegister.telephone_form_controller = function(evt) {
    var userId;

    var bindAddTelephone = function(){
      $('.user-add-telephone', $('.user-row')).on('click', openTelephoneForm);
      $('input[type="submit"]', $("#telephone-form")).on('click', hijackForm);
    };

    var openTelephoneForm = function(evt){
      getUserId($(evt.target));
      $('#telephone-form-group').show();
    };

    var getUserId = function(link){
      userId = $(link).parents('li').data('user');
      $('input[name="user_id"]', $("#telephone-form")).val(userId);
    };

    var hijackForm = function(evt) {
      evt.stopPropagation();
      evt.preventDefault();

      submitForm();
    };

    var submitForm = function(){
      var formData = getFormInputData();

      $.ajax({
        url: $('#telephone-form').attr('action'),
        type: 'POST',
        dataType: "json",
        data: {
          telephones: formData,
          authenticity_token: $('meta[name="csrf-token"]').attr('content')
        },
        success: handleSuccess,
        error: handleError
      });
    };

    var handleSuccess = function(number) {
      $('input[name="user_id"]', $("#telephone-form")).val('');
      updateNumberList(number);
      alert("Telefone cadastrado com sucesso");
      $('#telephone-form-group').hide();
    };

    var updateNumberList = function(number){
      var numberHtml = '' +
      '<li>' +
      '  <div class="user-number">Número: '+number.telephone_number+'</div>' +
      '  <div class="created_at">Criado em: '+formatDate(number.created_at)+'</div>' +
      '</li>';

      $("#number-list").append(numberHtml);
    };

    var formatDate = function(created_at){
      var date = new Date(created_at);
      return date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear();
    };

    var handleError = function(error){
      var errorsArray = $.parseJSON(error.responseText);
      showErrors(errorsArray.errors);
      alert("Ops houve um erro");
    };

    var getFormInputData = function(){
      var values = {};
      $.each($('#telephone-form').serializeArray(), function(i, field) {
          values[field.name] = field.value;
      });
      return values;
    };

    var showErrors = function(errors){
      $.each(errors, function(field, error) {
        $('input[name="'+field+'"]', $('#telephone-form')).addClass("field-with-error");
      });
    };

    return {
      init:  bindAddTelephone
    };
  };


})(jQuery);
