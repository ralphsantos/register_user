(function($){

  $.userRegister = $.userRegister || {};

  $.userRegister.user_form_controller = function(evt) {
    var bindUserForm = function(){
      $('input[type="submit"]', $("#user-form")).on('click', hijackForm);
      $("#add-new-user").on('click', showForm);
      $(".user-show-telephone").on('click', showUserTelephones);
    };

    var showForm = function(){
      $("#user-form-group").show();
    };

    var hijackForm = function(evt) {
      evt.stopPropagation();
      evt.preventDefault();

      submitForm();
    };

    var showUserTelephones = function(evt){
      var userId = getUserId($(evt.target));
      getTelephones(userId);
    };

    var getUserId = function(link){
      return $(link).parents('li').data('user');
    };

    var submitForm = function(){
      var formData = getFormInputData();

      $.ajax({
        url: $('#user-form').attr('action'),
        type: 'POST',
        dataType: "json",
        data: {
          user: formData,
          authenticity_token: $('meta[name="csrf-token"]').attr('content')
        },
        success: handleSuccess,
        error: handleError
      });
    };

    var getTelephones = function(userId){
      $.ajax({
        url: '/user-telephones',
        type: 'GET',
        dataType: "json",
        data: {
          user_id: userId,
          authenticity_token: $('meta[name="csrf-token"]').attr('content')
        },
        success: showUserTelephonesHandleSuccess,
        error: showUserTelephonesHandleError
      });
    };

    var showUserTelephonesHandleSuccess = function(numbers_array){
      var numbersHtml = '';
      $("#user-number-group").show();

      $.each(numbers_array, function(i, number) {
        var rowNumber = '' +
        '<li>' +
        '  <div class="user-number">Número: '+number.telephone_number+'</div>' +
        '  <div class="created_at">Criado em: '+formatDate(number.created_at)+'</div>' +
        '</li>';
        numbersHtml = numbersHtml + rowNumber;
      });

      $("ul", $("#user-number-group")).html(numbersHtml);
    };

    var formatDate = function(created_at){
      var date = new Date(created_at);
      return date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear();
    };

    var showUserTelephonesHandleError = function(){
      alert("Ops houve um erro, tente novamente");
    };

    var handleSuccess = function(user) {
      updateUserList(user);
      alert("Usuário cadastrado com sucesso");
    };

    var updateUserList = function(user){
      var userHtml = '' +
      '<li class="user-row" data-user="'+user.id+'">' +
      '  <div class="user-name"> '+user.email+'</div>' +
      '  <div class="user-name"> '+user.name+' </div>' +
      '  <a href="#" class="user-links user-show-telephone">show</a>' +
      '  <a href="#" class="user-links user-add-telephone">add</a>' +
      '</li>';

      $('.users-grid').append(userHtml);
    };

    var handleError = function(error){
      var errorsArray = $.parseJSON(error.responseText);
      showErrors(errorsArray.errors);
      alert("Ops houve um erro");
    };

    var getFormInputData = function(){
      var values = {};
      $.each($('#user-form').serializeArray(), function(i, field) {
          values[field.name] = field.value;
      });
      return values;
    };

    var showErrors = function(errors){
      $.each(errors, function(field, error) {
        $('input[name="'+field+'"]', $('#user-form')).addClass("field-with-error");
      });
    };

    return {
      init: bindUserForm
    };
  };

})(jQuery);
