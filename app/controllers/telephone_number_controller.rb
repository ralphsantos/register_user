class TelephoneNumberController < ApplicationController
  respond_to :html, :only => [:index]
  respond_to :json, :only => [:create]

  def index
    @telephones = TelephoneNumber.all
  end

  def create
    @tel = TelephoneNumber.create(telephone_params)

    respond_with @tel do |format|
      format.html {redirect_to root_path}
    end
  end

  def user_numbers
    @telephones = TelephoneNumber.where(user_id: params[:user_id].to_i)
    render json: @telephones.to_json
  end

  private

  def telephone_params
    params.require(:telephones).permit(:user_id, :telephone_number)
  end

end
