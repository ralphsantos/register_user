class UserController < ApplicationController
  respond_to :html, :only => [:index]
  respond_to :json, :only => [:create, :update]

  def index
    @users = User.all
  end

  def create
    @user = User.new(user_params)


    respond_with @user do |format|
      format.html {redirect_to root_path}
      if @user.save
        format.json { render json: @user.to_json, status: :created}
      else
        format.json { render json: @user.to_json, status: :unprocessable_entity}
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :address, :number, :neighbourhood, :city, :country, :uf)
  end

end
