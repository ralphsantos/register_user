# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  email         :string           default(""), not null
#  name          :string           default(""), not null
#  address       :string
#  number        :string
#  neighbourhood :string
#  complement    :string
#  uf            :string
#  city          :string
#  country       :string
#  created_at    :datetime
#  updated_at    :datetime
#

class User < ActiveRecord::Base
  has_many :telephone_numbers
  validates :email, :name, :address, :number, :neighbourhood, :city, :country , :uf, presence: true
end
