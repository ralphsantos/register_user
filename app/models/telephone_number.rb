# == Schema Information
#
# Table name: telephone_numbers
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  telephone_number :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class TelephoneNumber < ActiveRecord::Base
  belongs_to :user
  validates :telephone_number, length: { in: 10..13 }
end
